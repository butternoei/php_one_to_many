<?php
require_once('../config.php');

if($_POST) {
   $r_title = $_POST['r_title'];
   $r_status = $_POST['r_status'];
   $r_funding = $_POST['r_funding'];
   $r_start = $_POST['r_start'];
   
   $researchers = json_decode($_POST['researchers']);

   $sql_research = "INSERT INTO research (r_title, r_status, r_funding, r_start) VALUES ('$r_title', '$r_status', '$r_funding', '$r_start')";
   $query_research = mysqli_query($conn, $sql_research);

   if($query_research) {
      $r_id = mysqli_insert_id($conn);
      
      foreach($researchers as $value) {
         $rcher_share = $value->rcher_share;
         $user_id = $value->user_id;

         $sql_researcher = "INSERT INTO researcher (r_id, rcher_share, user_id) VALUES ('$r_id', '$rcher_share', '$user_id')";
         $query_researcher = mysqli_query($conn, $sql_researcher);

         if(!$query_researcher) {
            echo json_encode([
               "result" => "failed",
               "msg" => "Create researcher error: " . mysqli_error($conn)
            ]);
            return;
         }
      }

      echo json_encode([
         "result" => "success"
      ]);
   } else {
      echo json_encode([
         "result" => "failed",
         "msg" => "Create research error: " . mysqli_error($conn)
      ]);
   }
} else {
   header('../../index.php');
}

mysqli_close($conn);