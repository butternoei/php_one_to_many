<?php
require_once('../config.php');

if ($_POST['r_id']) {
   $r_id = $_POST['r_id'];
   $r_title = $_POST['r_title'];
   $r_status = $_POST['r_status'];
   $r_funding = $_POST['r_funding'];
   $r_start = $_POST['r_start'];
   $r_file = $_FILES['r_file'];

   $sql_research = "UPDATE research 
                     SET r_title = '$r_title', 
                        r_status = '$r_status', 
                        r_funding = '$r_funding', 
                        r_start = '$r_start'
                     WHERE r_id = '$r_id'";
   $query_research = mysqli_query($conn, $sql_research);

   if ($query_research) {
      // ถ้ามีการ upload ไฟล์
      if ($r_file['tmp_name'] != '') {
         $r_file_rename = 'research_' . uniqid() . '.' . pathinfo($r_file['name'], PATHINFO_EXTENSION); //เปลี่ยนชื่อไฟล์

         // หาไฟล์เก่า
         $sql_old_file = "SELECT r_file FROM research WHERE r_id = '$r_id' AND r_file != ''";
         $old_file_query = mysqli_query($conn, $sql_old_file);

         // ถ้ามีไฟล์เก่า
         if (mysqli_num_rows($old_file_query) > 0) {
            // Fetch มันออกมาแล้วก็ลบไฟล์ทิ้งด้วยคำสั่ง unlink ซะ!
            $old_file = mysqli_fetch_assoc($old_file_query);
            unlink('../../assets/file/research/' . $old_file['r_file']);
         }

         // อัพโหลดภาพ
         if (move_uploaded_file($r_file['tmp_name'], '../../assets/file/research/' . $r_file_rename)) {
            $sql_update_file = "UPDATE research SET r_file = '$r_file_rename' WHERE r_id = '$r_id'";
            $query_update_file = mysqli_query($conn, $sql_update_file);

            // ถ้า update ไม่สำเร็จ
            if (!$query_update_file) {
               echo json_encode([
                  "result" => "failed",
                  "msg" => "Update file research error: " . mysqli_error($conn)
               ]);
               return;
            }
         } else {
            echo json_encode([
               "result" => "upload_failed",
               "msg" => "Fail upload file"
            ]);
            return;
         }
      }

      echo json_encode([
         "result" => "success"
      ]);
   } else {
      echo json_encode([
         "reult" => "failed",
         "msg" => "Update research error: " . mysqli_error($conn)
      ]);
   }
} else {
   header('../../index.php');
}

mysqli_close($conn);
