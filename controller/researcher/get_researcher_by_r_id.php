<?php
require_once('../config.php');

if(isset($_GET['r_id'])) {
   $r_id = $_GET['r_id'];

   $sql_researcher = "SELECT rcher_id, r_id, rcher_share, user.user_id, username, rank, user_img, user_fname, user_lname, user_department, user_tel, user_email, user_url, user_type FROM researcher
                     LEFT JOIN user
                     ON researcher.user_id = user.user_id
                     WHERE r_id = '$r_id'";
   $query_researcher = mysqli_query($conn, $sql_researcher);
   $researchers = [];

   if($query_researcher) {
      while($row = mysqli_fetch_assoc($query_researcher)) {
         array_push($researchers, $row);
      }

      echo json_encode([
         "result" => "success",
         "data" => $researchers
      ]);
   } else {
      echo json_encode([
         "result" => "failed",
         "msg" => "Read researcher error: " . mysqli_error($conn)
      ]);
   }
} else {
   header('../../index.php');
}

mysqli_close($conn);