<?php
require_once('../config.php');

if(isset($_GET['rcher_id'])) {
   $rcher_id = $_GET['rcher_id'];

   $sql_researcher = "SELECT * FROM researcher
                     LEFT JOIN user
                     ON researcher.user_id = user.user_id
                     WHERE rcher_id = '$rcher_id'";
   $query_researcher = mysqli_query($conn, $sql_researcher);

   if($query_researcher) {
      $researcher = mysqli_fetch_assoc($query_researcher);

      echo json_encode([
         "result" => "success",
         "data" => $researcher
      ]);
   } else {
      echo json_encode([
         "result" => "failed",
         "msg" => "Read researcher error: " . mysqli_error($conn)
      ]);
   }
} else {
   header('../../index.php');
}

mysqli_close($conn);