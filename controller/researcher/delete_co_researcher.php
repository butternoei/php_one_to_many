<?php
require_once('../config.php');

if(isset($_GET['rcher_id'])) {
   $rcher_id = $_GET['rcher_id'];

   $sql_researcher = "DELETE FROM researcher WHERE rcher_id = '$rcher_id'";
   $query_researcher = mysqli_query($conn, $sql_researcher);

   if($query_researcher) {
      echo json_encode([
         "result" => "success"
      ]);
   } else {
      echo json_encode([
         "result" => "failed",
         "msg" => "Read researcher error: " . mysqli_error($conn)
      ]);
   }
} else {
   header('../../index.php');
}

mysqli_close($conn);