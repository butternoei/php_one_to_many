<?php
require_once('controller/config.php');

$sql_user = "SELECT * FROM user";
$query_user = mysqli_query($conn, $sql_user);
?>
<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>One to many</title>

   <?php include_once('common/style/style.inc.php') ?>
   <link rel="stylesheet" href="assets/vendor/select-picker/bootstrap-select.min.css">
   <style>
      .cursor-pointer {
         cursor: pointer;
      }
   </style>
</head>

<body>
   <div class="container pt-5" style="max-width: 600px;">
      <div class="text-center">
         <h3>เพิ่มข้อมูลงานวิจัย</h3>
      </div>
      <form id="form-create-research">
         <input type="hidden" name="r_status" value="ดำเนินการ">
         <div class="form-group">
            <label for="r_title">Title</label>
            <input type="text" name="r_title" id="r_title" class="form-control" placeholder="r_title" required>
         </div>
         <div class="form-group">
            <label for="r_funding">Funding</label>
            <input type="number" name="r_funding" id="r_funding" class="form-control" placeholder="funding" required>
         </div>
         <div class="form-group">
            <label for="r_start">Start</label>
            <input type="date" name="r_start" id="r_start" class="form-control" placeholder="r_start" required>
         </div>
         <div class="form-group">
            <label for="r_file">File</label>
            <input type="file" name="r_file" id="r_file" class="form-control" placeholder="r_file">
         </div>
         <div class="form-group">
            <div class="form-row">
               <div class="col-md-6">
                  <label for="user_id">นักวิจัย</label>
                  <select id="user_id" class="form-control selectpicker" data-live-search="true">
                     <option selected disabled>เลือกนักวิจัย</option>
                     <?php while ($row = mysqli_fetch_assoc($query_user)) { ?>
                        <option value="<?= $row['user_id'] ?>"><?= $row['user_fname'] . ' ' . $row['user_lname'] ?></option>
                     <?php } ?>
                  </select>
               </div>
               <div class="col-md-6">
                  <label for="rcher_share">ส่วนแบ่ง %</label>
                  <input type="number" id="rcher_share" step="any" min="0" max="100" class="form-control" placeholder="0">
               </div>
            </div>
            <div class="card mt-3">
               <!-- Show researcher List -->
               <ul id="author-list" class="list-group list-group-flush">

               </ul>
            </div>
            <div class="text-center pt-2">
               <!-- Button add researcher -->
               <button type="button" class="btn btn-warning rounded-circle btn-add-research" title="เพิ่มนักวิจัย">
                  <i class="fas fa-plus text-white"></i>
               </button>
            </div>
         </div>
         <div class="text-right">
            <button type="submit" class="btn btn-primary">บันทึก</button>
         </div>
      </form>
   </div>

   <?php include_once('common/script/script.inc.php') ?>
   <script src="assets/vendor/select-picker/bootstrap-select.min.js"></script>
   <script src="assets/js/research/research.js"></script>
   <script>
      $('.selectpicker').selectpicker()
   </script>
</body>

</html>