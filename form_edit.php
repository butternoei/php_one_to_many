<?php
require_once('controller/config.php');

if (isset($_GET['r_id'])) {
   $r_id = $_GET['r_id'];

   $sql_user = "SELECT * FROM user";
   $query_user = mysqli_query($conn, $sql_user);
   $users = [];

   while ($row = mysqli_fetch_assoc($query_user)) {
      array_push($users, $row);
   }

   $sql_research = "SELECT * FROM research WHERE r_id = '$r_id'";
   $query_research = mysqli_query($conn, $sql_research);
   $row_research = mysqli_fetch_assoc($query_research);

   $sql_researcher = "SELECT * FROM researcher 
                     LEFT JOIN user 
                     ON researcher.user_id = user.user_id
                     WHERE r_id = '$r_id'";
   $query_researcher = mysqli_query($conn, $sql_researcher);
} else {
   header('Location: index.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>One to many</title>

   <?php include_once('common/style/style.inc.php') ?>
   <link rel="stylesheet" href="assets/vendor/select-picker/bootstrap-select.min.css">
   <style>
      .cursor-pointer {
         cursor: pointer;
      }
   </style>
</head>

<body>
   <div class="container py-5" style="max-width: 600px;">
      <div class="text-center">
         <h3>แก้ไขข้อมูลงานวิจัย</h3>
      </div>
      <div class="text-right">
         <a href="index.php">ย้อนกลับ</a>
      </div>
      <form id="form-edit-research">
         <input type="hidden" id="r_id" name="r_id" value="<?= $row_research['r_id'] ?>">
         <div class="form-group">
            <label for="r_title">Title</label>
            <input type="text" name="r_title" id="r_title" class="form-control" value="<?= $row_research['r_title'] ?>" placeholder="r_title" required>
         </div>
         <div class="form-group">
            <label for="r_funding">Funding</label>
            <input type="number" name="r_funding" id="r_funding" class="form-control" value="<?= $row_research['r_funding'] ?>" placeholder="funding" required>
         </div>
         <div class="form-group">
            <label for="r_start">Start</label>
            <input type="date" name="r_start" id="r_start" class="form-control" value="<?= $row_research['r_start'] ?>" placeholder="r_start" required>
         </div>
         <div class="form-group">
            <label for="r_status">Status</label>
            <select name="r_status" id="r_status" class="form-control" required>
               <option value="ดำเนินการ" <?= $row_research['r_status'] == 'ดำเนินการ' ? 'selected' : null ?>>ดำเนินการ</option>
               <option value="เสร็จสิ้น" <?= $row_research['r_status'] == 'เสร็จสิ้น' ? 'selected' : null ?>>เสร็จสิ้น</option>
            </select>
         </div>
         <div class="form-group">
            <label for="r_file">File</label>
            <input type="file" name="r_file" id="r_file" class="form-control" placeholder="r_file">
         </div>
         <div class="form-group">
            <div class="form-row">
               <div class="col-md-6">
                  <label for="user_id">นักวิจัย</label>
                  <select id="user_id" class="form-control selectpicker" data-live-search="true">
                     <option selected disabled>เลือกนักวิจัย</option>
                     <?php foreach ($users as $value) { ?>
                        <option value="<?= $value['user_id'] ?>"><?= $value['user_fname'] . ' ' . $value['user_lname'] ?></option>
                     <?php } ?>
                  </select>
               </div>
               <div class="col-md-6">
                  <label for="rcher_share">ส่วนแบ่ง %</label>
                  <input type="number" id="rcher_share" step="any" min="0" max="100" class="form-control" placeholder="0">
               </div>
            </div>
            <div class="card mt-3">
               <!-- Show researcher List -->
               <ul id="author-list" class="list-group list-group-flush">
                  <?php while ($row = mysqli_fetch_assoc($query_researcher)) { ?>
                     <li class="list-group-item d-flex align-items-center">
                        <span><?= $row['user_fname'] . ' ' . $row['user_lname'] ?></span>
                        <span class="ml-auto">
                           <?= $row['rcher_share'] . '%' ?>
                        </span>
                        <div class="ml-2">
                           <span class="btn-edit-researcher" data-id="<?= $row['rcher_id'] ?>">
                              <i class="fas fa-edit cursor-pointer text-primary"></i>
                           </span>
                           <span class="btn-delete-researcher" data-id="<?= $row['rcher_id'] ?>" data-userId="<?= $row['rcher_id'] ?>">
                              <i class="fa fa-trash-alt cursor-pointer text-danger"></i>
                           </span>
                        </div>
                     </li>
                  <?php } ?>
               </ul>
            </div>
            <div class="text-center pt-2">
               <!-- Button add researcher -->
               <button type="button" class="btn btn-warning rounded-circle btn-add-research" title="เพิ่มนักวิจัย">
                  <i class="fas fa-plus text-white"></i>
               </button>
            </div>
         </div>
         <div class="text-right">
            <button type="submit" class="btn btn-primary">บันทึก</button>
         </div>
      </form>
   </div>

   <!-- Modal edit research -->
   <div class="modal fade" id="editResearchModal" tabindex="-1" role="dialog" aria-labelledby="editResearchModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg">
         <div class="modal-content">
            <div class="modal-header">
               <h5 class="modal-title" id="editResearchModalLabel">แก้ไขนักวิจัย</h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <form id="form-edit-researcher">
               <div class="modal-body">
                  <input type="hidden" id="rcher_edit_id" name="rcher_id" value="">
                  <div class="form-group">
                     <div class="form-row">
                        <div class="col-md-6">
                           <label for="user_edit_id">นักวิจัย</label>
                           <select id="user_edit_id" name="user_id" class="form-control selectpicker" data-live-search="true">
                              <option selected disabled>เลือกนักวิจัย</option>
                              <?php foreach ($users as $value) { ?>
                                 <option value="<?= $value['user_id'] ?>"><?= $value['user_fname'] . ' ' . $value['user_lname'] ?></option>
                              <?php } ?>
                           </select>
                        </div>
                        <div class="col-md-6">
                           <label for="rcher_edit_share">ส่วนแบ่ง %</label>
                           <input type="number" name="rcher_share" id="rcher_edit_share" step="any" min="0" max="100" class="form-control" placeholder="0">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="modal-footer">
                  <button type="reset" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                  <button type="submit" id="btn-submit-edit-researcher" class="btn btn-primary">บันทึก</button>
               </div>
            </form>
         </div>
      </div>
   </div>

   <?php include_once('common/script/script.inc.php') ?>
   <script src="assets/vendor/select-picker/bootstrap-select.min.js"></script>
   <script src="assets/js/research/research.js"></script>
   <script>
      $('.selectpicker').selectpicker()
      $(document).ready(function() {
         var r_id = $('#r_id').val()

         $.ajax({
            url: 'controller/researcher/get_researcher_by_r_id.php',
            data: 'r_id=' + r_id,
            method: 'GET',
            dataType: 'JSON',
            success: function(res) {
               if (res.result == 'success') {
                  var author_list = res.data.map(author => {
                     researchers.push({ user_id: author.user_id, rcher_share: author.rcher_share })
                  })
               } else {
                  // ===== Fix =====
                  alert('เกิดข้อผิดพลาดในการร้องข้อมูล')
                  // ===== Fix =====
               }
            },
            error: function() {
               // ===== Fix =====
               alert('Error')
               // ===== Fix =====
            }
         })
      })
   </script>
</body>

</html>