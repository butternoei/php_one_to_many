function showResearcherList(user_name, rcher_share) {
   $('#author-list').append(`
      <li class="list-group-item d-flex">
         <span>${user_name}</span>
         <span class="ml-auto">${rcher_share}%</span>
         <div class="ml-2">
            <span class="btn-remove-researcher" data-id="">
               <i class="fa fa-trash-alt cursor-pointer text-danger"></i>
            </span>
         </div>
      </li>
   `)
}

// Global varliable
var researchers = []
var sumShare = 0

function checkSharePercent() {
   var rcher_share = parseFloat($('#rcher_share').val())

   // เช็คว่ามีค่าใน researchers หรือยัง
   if (typeof researchers[0] != 'undefined') {
      // รวมค่า share โดยใช้ reduce ช่วย
      sumShare = researchers.reduce((total, researcher) => parseFloat(researcher.rcher_share) + total, 0)
      var currentPercent = rcher_share + sumShare

      // เช็คว่าค่าที่ user กรอกเข้ามา พอเอามารวมกับค่าที่มีอยู่ใน researchers แล้วมันเกิน 100 รึยัง
      if (currentPercent > 100) {
         $('.btn-add-research').attr('disabled', true) // คำสั่ง disable ปุ่ม
         return false
      }
   }

   $('.btn-add-research').removeAttr('disabled')
   return true
}

function checkSameResearcher(id) {
   return researchers.filter(({ user_id }) => [id].includes(user_id))
}

// ทุกครั้งที่มีการพิมพ์ตัวเลขมันจะเช็คค่าตลอดใช้ keyup ในการดัก Event
$('#rcher_share').keyup(function () {
   checkSharePercent()
})

$('.btn-add-research').click(function () {
   if (checkSharePercent()) {
      var user_id = $('select#user_id').val()
      var user_name = $('select#user_id option:selected').html()
      var rcher_share = $('input#rcher_share').val()

      // เช็คว่า user เลือกนักวิจัยและกรอกข้อมูลครบทั้ง 2 ช่องมั้ย
      if (checkSameResearcher(user_id).length < 1) {
         if (user_id != null && rcher_share != '') {
            researchers.push({ user_id, rcher_share })
            showResearcherList(user_name, rcher_share)
         }
      }
   }
})

$('#form-create-research').submit(function (e) {
   e.preventDefault()

   var data = new FormData(this)
   data.append('researchers', JSON.stringify(researchers))

   // เช็คว่ามีค่าใน researchers หรือยัง
   // ถ้าไม่มีไม่ยอมให้ ajax ยิง request
   if (typeof researchers[0] != 'undefined') {
      $.ajax({
         url: 'controller/research/create_research.php',
         data: data,
         cache: false,
         processData: false,
         contentType: false,
         method: 'POST',
         dataType: 'JSON',
         success: function (res) {
            // ===== Fix =====
            alert('Research created')
            // ===== Fix =====
         },
         error: function () {
            // ===== Fix =====
            alert('Error')
            // ===== Fix =====
         },
      })
   } else {
      // ===== Fix =====
      alert('Please enter percent share')
      // ===== Fix =====
   }
})

// ====== Update Researcher ======
$('#form-edit-research').submit(function (e) {
   e.preventDefault()

   var data = new FormData(this)

   $.ajax({
      url: 'controller/research/update_research.php',
      data: data,
      cache: false,
      processData: false,
      contentType: false,
      method: 'POST',
      dataType: 'JSON',
      success: function (res) {
         // ===== Fix =====
         alert('Research updated')
         window.location = 'index.php'
         // ===== Fix =====
      },
      error: function () {
         // ===== Fix =====
         alert('Error')
         // ===== Fix =====
      },
   })
})

$(document).on('click', '.btn-edit-researcher', function () {
   var rcher_id = $(this).data('id')

   $.ajax({
      url: 'controller/researcher/get_researcher_by_rcher_id.php',
      data: 'rcher_id=' + rcher_id,
      method: 'GET',
      dataType: 'JSON',
      success: function (res) {
         if (res.result == 'success') {
            var { rcher_id, user_id, rcher_share } = res.data

            // Add data to input
            $('input#rcher_edit_id').val(rcher_id)
            $(`#form-edit-researcher select#user_edit_id option`).removeAttr('selected')
            $(`#form-edit-researcher select#user_edit_id option[value="${user_id}"]`).attr('selected', true)
            $('input#rcher_edit_share').val(rcher_share)
            $('.selectpicker').selectpicker('refresh')

            $('#editResearchModal').modal('show')
         } else {
            // ===== Fix =====
            alert('เกิดข้อผิดพลาดในการร้องข้อมูล')
            // ===== Fix =====
         }
      },
      error: function () {
         // ===== Fix =====
         alert('Error')
         // ===== Fix =====
      },
   })
})

function resetResearchList(r_id) {
   $.ajax({
      url: 'controller/researcher/get_researcher_by_r_id.php',
      data: 'r_id=' + r_id,
      method: 'GET',
      dataType: 'JSON',
      success: function (res) {
         if (res.result == 'success') {
            var researchers = res.data
            var rcherList = researchers.map(
               (researcher) => `
               <li class="list-group-item d-flex align-items-center">
                  <span>${researcher.user_fname + ' ' + researcher.user_lname}</span>
                  <span class="ml-auto">
                     ${researcher.rcher_share}%
                  </span>
                  <div class="ml-2">
                     <span class="btn-edit-researcher" data-id="${researcher.rcher_id}">
                        <i class="fas fa-edit cursor-pointer text-primary"></i>
                     </span>
                     <span class="btn-delete-researcher" data-id="${researcher.rcher_id}">
                        <i class="fa fa-trash-alt cursor-pointer text-danger"></i>
                     </span>
                  </div>
               </li>
            `
            )

            $('#author-list').html('')
            $('#author-list').append(rcherList)
         } else {
            // ===== Fix =====
            alert('เกิดข้อผิดพลาดในการร้องข้อมูล')
            // ===== Fix =====
         }
      },
      error: function () {
         // ===== Fix =====
         alert('Error')
         // ===== Fix =====
      },
   })
}

$('#form-edit-researcher').submit(function (e) {
   e.preventDefault()

   var data = $(this).serialize()

   $.ajax({
      url: 'controller/researcher/update_researcher.php',
      data: data,
      method: 'POST',
      dataType: 'JSON',
      success: function (res) {
         var r_id = $('input#r_id').val()
         resetResearchList(r_id)

         // ===== Fix =====
         alert('Researcher updated')
         // ===== Fix =====
      },
      error: function () {
         // ===== Fix =====
         alert('Error')
         // ===== Fix =====
      },
   })
})

$(document).on('click', '.btn-delete-researcher', function () {
   var rcher_id = $(this).data('id')
   var user_id = $(this).data('userId')

   // Fix กดยืนยันก่อนแล้วค่อยลบ
   $.ajax({
      url: 'controller/researcher/delete_co_researcher.php',
      method: 'GET',
      data: 'rcher_id=' + rcher_id,
      dataType: 'JSON',
      success: function (res) {
         var r_id = $('input#r_id').val()
         researchers = researchers.filter(researcher => researcher.user_id !== user_id)
         resetResearchList(r_id)

         // ===== Fix =====
         alert('Researcher deleted')
         // ===== Fix =====
      },
      error: function () {
         // ===== Fix =====
         alert('Error')
         // ===== Fix =====
      },
   })
})

$(document).on('click', '.btn-remove-researcher', function () {
   $(this).unwrap()
   $(this).parent('li.list-group-item').remove()
})
