<?php
require_once('controller/config.php');

$sql_research = "SELECT * FROM research";
$query_research = mysqli_query($conn, $sql_research);
?>
<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>One to many</title>

   <?php include_once('common/style/style.inc.php') ?>
   <link rel="stylesheet" href="assets/vendor/select-picker/bootstrap-select.min.css">
   <style>
      .cursor-pointer {
         cursor: pointer;
      }
   </style>
</head>

<body>
   <div class="container pt-5">
      <div class="text-center">
         <h3>เพิ่มข้อมูลงานวิจัย</h3>
      </div>
      <div class="text-right">
         <a href="form_create.php">เพิ่มงานวิจัย</a>
      </div>
      <div class="table-responsive">
         <table class="table table-bordered">
            <thead>
               <th>r_title</th>
               <th>r_status</th>
               <th>r_funding</th>
               <th>r_file</th>
               <th class="text-center">ตัวเลือก</th>
            </thead>
            <tbody>
               <?php while ($row = mysqli_fetch_assoc($query_research)) { ?>
                  <tr>
                     <td><?= $row['r_title'] ?></td>
                     <td><?= $row['r_status'] ?></td>
                     <td><?= $row['r_funding'] ?></td>
                     <td>
                        <a href="assets/file/research/<?= $row['r_file'] ?>" download><?= $row['r_file'] ?></a>
                     </td>
                     <td class="text-center">
                        <a href="form_edit.php?r_id=<?= $row['r_id'] ?>"><i class="fas fa-edit"></i></a>
                        <i class="fa fa-trash-alt text-danger cursor-pointer btn-delete-research"></i>
                     </td>
                  </tr>
               <?php } ?>
            </tbody>
         </table>
      </div>
   </div>

   <?php include_once('common/script/script.inc.php') ?>
   <script src="assets/vendor/select-picker/bootstrap-select.min.js"></script>
   <script src="assets/js/research/research.js"></script>
   <script>
      $('.selectpicker').selectpicker()
   </script>
</body>

</html>